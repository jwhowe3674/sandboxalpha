<?php

use Illuminate\Database\Seeder;
use App\User;

class DefaultUsersSeeder extends Seeder
{
    /**
     * Run the database seeds to generate two default users:
     * User 1: git@mcclainconcepts.com
     * User 2: jwhowe2007@gmail.com
     * Both users use the following password: LaravelTestPW
     *
     * @return void
     */
    protected $testPassword = 'LaravelTestPW';

    public function run()
    {
        $gitMcCCUser = factory(App\User::class)->create([
          'name' => 'gitMcCC',
          'email' => 'git@mcclainconcepts.com',
          'password' => bcrypt($this->testPassword)
        ]);

        $myUser = factory(App\User::class)->create([
          'name' => 'jwhowe2007',
          'email' => 'jwhowe2007@gmail.com',
          'password' => bcrypt($this->testPassword)
        ]);
    }
}
