@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Justin's Test App</div>

                <div class="panel-body">
                    <strong>Version 0.1</strong>
                    <div class="row">
                      <div class="col-md-4">
                        Justin W. Howe<br />
                        Software Engineer<br />
                        Designer, developer and maintainer<br />
                        Constructed with Laravel and the Atom IDE.<br />

                        {!! HTML::image('assets/images/JustinWHowe.jpg', 'Self Portrait') !!}
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
